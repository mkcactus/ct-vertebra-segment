import numpy as np
import matplotlib.pyplot as mplt
import cv2
import DataLoader as dl
import GenerateSeeds as gs
import GraphCut as gc
import PostProcess as pp


SEG_WIN_NAME = "Vertebra Segments"
SEG_TRACKBAR_NAME = "Z Level"
MAIN_WIN_NAME = "Full CT"
MAIN_TRACKBAR_NAME = "CT slice"
DATA_PATH = "../NEW_CT/Pancreas-CT/PANCREAS_0010/" #"../CT_DATA/DICOM/PA1/ST1/SE2/"

AVG_WIN_SIZE = 10
AVG_DIFF_TH = 4
DFC_TH = 50
INIT_TH = 0
TEMPL_RAD = 70

VIEW = 0
SHOW_BAD = False


def on_seg_trackbar(val):
    sl = cut[:, :, val]
    cv2.imshow(SEG_WIN_NAME, sl)


def on_main_trackbar(val):
    if VIEW==0:
        sl = orig[:, :, val]
        good = marked_good[:, :, val]
        bad = marked_bad[:,:,val]
        label = labels[:,:,val]
    elif VIEW==1:
        sl = orig[:, val, :]
        good = marked_good[:, val, :]
        bad = marked_bad[:,val,:]
        label = labels[:,val,:]
    elif VIEW==2:
        sl = orig[val, :, :]
        good = marked_good[val, :, :]
        bad = marked_bad[val,:,:]
        label = labels[val,:,:]
    # setup 3 channels
    good = np.reshape(good, (good.shape[0], good.shape[1], 1))
    bad = np.reshape(bad, (bad.shape[0], bad.shape[1], 1))
    temp = np.copy(sl)
    temp = np.reshape(temp, (temp.shape[0], temp.shape[1], 1))
    if SHOW_BAD:
        img = np.concatenate((temp, good, bad), axis=2)
    else:
        img = np.concatenate((temp, good, temp), axis=2)

    for i in xrange(len(cuts)):
        l_ind = np.argwhere(label==i+1)
        if l_ind.size != 0:
            point = l_ind[0]
            cv2.putText(img, str(i+1), (point[1],point[0]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,255), 3)
    cv2.imshow(MAIN_WIN_NAME, img)

def make_view(max_slice):
    cv2.namedWindow(MAIN_WIN_NAME)
    cv2.createTrackbar(MAIN_TRACKBAR_NAME, MAIN_WIN_NAME, 0, max_slice, on_main_trackbar)
    on_main_trackbar(0)

def plot_data(avg, dfc):
    avgs = np.ma.array(avg)
    avgs_masked = np.ma.masked_where(avgs > AVG_DIFF_TH, avgs)  # find minima
    mplt.plot(avgs)
    mplt.plot(avgs_masked, 'r-')
    mplt.title("Average distance in a window (size: {}, threshold: {})".format(AVG_WIN_SIZE, AVG_DIFF_TH))
    mplt.show()

    dfcs = np.ma.array(dfc)
    dfc_masked = np.ma.masked_where(dfcs > DFC_TH, dfcs)  # find minima
    mplt.plot(dfcs)
    mplt.plot(dfc_masked, 'r-')
    mplt.title("Distance from centroid of candidate seeds (threshold: {})".format(DFC_TH))
    mplt.show()


orig = dl.load_folder_raw(DATA_PATH)
dl.convert_volume(orig, INIT_TH)
orig = orig.astype(np.uint8)
centers = dl.read_centers(DATA_PATH+"tm.center")
if len(centers) == 0:
    print "Center finding..."
    centers = gs.match_all_template(orig, TEMPL_RAD)
    dl.make_center_file(centers, DATA_PATH+"tm.center")
    centers = dl.read_centers(DATA_PATH+"tm.center")
print "Average distance window: "+str(AVG_WIN_SIZE)
avgs = gs.windowed_avg_diff(centers, AVG_WIN_SIZE)
print "Average distance threshold: "+str(AVG_DIFF_TH)
candidate_indexes = np.where(avgs < AVG_DIFF_TH)[0]
candidate_seeds = centers[candidate_indexes]

print "Distance from centroid threshold: "+str(DFC_TH)
dfc = gs.diff_from_centroid(candidate_seeds)
seed_indexes = candidate_indexes[np.where(dfc < DFC_TH)[0]]

print "Graph cutting..."
# For single graph cutting:
# roi, roi_seeds = Segmenter.vert_roi_cut(orig, seed_indexes, centers[seed_indexes])
# cut = gc.build_graph(roi, roi_seeds)

# For full graph cutting:
rois, roiseeds, roistarts, roiends = gs.roi_cut(orig, seed_indexes, centers)
cuts = []
for i in xrange(len(rois)):
    print "Cut {}/{}".format(i+1, len(rois))
    cuts.append(gc.build_graph(cv2.GaussianBlur(rois[i], (7, 7), 20), roiseeds[i]))

# plotting:
# plot_data(avgs, dfc)
# gc.show_gaussian_fit(orig[:, :, 385], centers[385], sample_size=5)

marked = np.zeros_like(orig)
labels = np.zeros_like(orig)
for i in xrange(len(cuts)):
    st = roistarts[i]
    ed = roiends[i]
    marked[st[0]:ed[0], st[1]:ed[1], st[2]:ed[2]] = cuts[i]
    labels[st[0]:ed[0], st[1]:ed[1], st[2]:ed[2]] = i+1

print "Detected vertebrae: "+str(len(cuts))
print "Showing first vertebra (space for next vertebra, esc to quit)..."

num = 0
cut = cuts[num]
max_level = cut.shape[2]-1

marked_good, marked_bad = pp.filter_segm(marked, 0.8, (cut.shape[0]*cut.shape[1]))
marked_good = pp.clean_up_segm(marked_good)

cv2.namedWindow(SEG_WIN_NAME)
cv2.createTrackbar(SEG_TRACKBAR_NAME, SEG_WIN_NAME, 0, max_level, on_seg_trackbar)
on_seg_trackbar(0)
make_view(orig.shape[2]-1)

while True:
    key = cv2.waitKey(0)
    if key == 27:
        cv2.destroyWindow(SEG_WIN_NAME)
        cv2.destroyWindow(MAIN_WIN_NAME)
        break
    if key == 32:
        cv2.destroyWindow(SEG_WIN_NAME)
        num += 1
        if num == len(cuts):
            num = 0
        cut = cuts[num]
        max_level = cut.shape[2]-1
        cv2.namedWindow(SEG_WIN_NAME)
        cv2.createTrackbar(SEG_TRACKBAR_NAME, SEG_WIN_NAME, 0, max_level, on_seg_trackbar)
        on_seg_trackbar(0)
    if key == ord('0'):
        VIEW = 0
        cv2.destroyWindow(MAIN_WIN_NAME)
        make_view(orig.shape[2]-1)
    if key == ord('1'):
        VIEW = 1
        cv2.destroyWindow(MAIN_WIN_NAME)
        make_view(orig.shape[1]-1)
    if key == ord('2'):
        VIEW = 2
        cv2.destroyWindow(MAIN_WIN_NAME)
        make_view(orig.shape[0]-1)
    if key == ord('a'):
        SHOW_BAD = not SHOW_BAD
        pos = cv2.getTrackbarPos(MAIN_TRACKBAR_NAME, MAIN_WIN_NAME)
        on_main_trackbar(pos)