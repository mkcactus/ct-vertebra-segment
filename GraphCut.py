import cv2
import numpy as np
import maxflow
from matplotlib import pyplot as ppl


def gaussian(x, mu, sig):
    '''Simple normalized gaussian'''

    return 1/(sig * np.sqrt(2 * np.pi))*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


def grad_magn(img):
    '''Magnitude of gradient'''

    img = cv2.equalizeHist(img)
    dy = np.square(cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5))
    dx = np.square(cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5))
    return dx+dy
    # return np.sqrt(np.sum(np.square(np.array(np.gradient(img))), axis=0))


def sample_cut(roi, s, size):
    '''Selects a sample around the point "s"'''

    return roi[s[0]-size[0]:s[0]+size[0], s[1]-size[1]:s[1]+size[1], s[2]-size[2]:s[2]+size[2]]


def show_gaussian_fit(mat, point, sample_size=5):
    '''Debug info'''

    x, y = point
    sample = mat[x-sample_size:x+sample_size, y-sample_size:y+sample_size]

    ppl.subplot(231)
    ppl.imshow(mat, cmap=ppl.cm.gray)
    ppl.plot(x, y, 'ro')
    ppl.subplot(232)
    _, bins, _ = ppl.hist(sample.ravel(), density=True)
    ppl.plot(bins, gaussian(bins, np.mean(sample), np.std(sample)))
    ppl.title('mean:{} std:{}'.format(np.mean(sample), np.std(sample)))
    ppl.subplot(233)
    ppl.imshow(mat*gaussian(mat, np.mean(sample), np.std(sample)), cmap=ppl.cm.gray)

    ppl.subplot(234)
    y -= 65
    x -= 65
    sample = mat[x-sample_size:x+sample_size, y-sample_size:y+sample_size]
    ppl.imshow(mat, cmap=ppl.cm.gray)
    ppl.plot(x, y, 'ro')
    ppl.subplot(235)
    _, bins, _ = ppl.hist(sample.ravel(), density=True)
    ppl.plot(bins, gaussian(bins, np.mean(sample), np.std(sample)))
    ppl.title('mean:{} std:{}'.format(np.mean(sample), np.std(sample)))
    ppl.subplot(236)
    ppl.imshow((255-mat)*gaussian(mat, np.mean(sample), np.std(sample)), cmap=ppl.cm.gray)

    mng = ppl.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    ppl.show()


def build_graph_2d(img, seed, sample_size=10, anti_seed_offset=(0, -40)):
    '''Builds and cuts a 2D image graph (currently unused)'''

    x, y = seed
    x_off, y_off = anti_seed_offset
    sample = img[x-sample_size:x+sample_size, y-sample_size:y+sample_size]

    x += x_off
    y += y_off
    anti_sample = img[x-sample_size:x+sample_size, y-sample_size:y+sample_size]

    prior_fg = img*gaussian(img, np.mean(sample), np.std(sample))
    ppl.subplot(131)
    ppl.title("Foreground prior")
    ppl.imshow(prior_fg)
    prior_bg = (255-img)*gaussian(img, np.mean(anti_sample), np.std(anti_sample))
    ppl.subplot(132)
    ppl.title("Background prior")
    ppl.imshow(prior_bg)
    regul = grad_magn(img)
    ppl.subplot(133)
    ppl.title("Regularization")
    ppl.imshow(regul)
    ppl.show()

    graph = maxflow.Graph[float]()
    nodeids = graph.add_grid_nodes(img.shape)
    graph.add_grid_edges(nodeids, 255-regul)
    graph.add_grid_tedges(nodeids, prior_fg, prior_bg)
    graph.maxflow()
    sgm = graph.get_grid_segments(nodeids)
    img2 = np.int_(np.logical_not(sgm))
    return np.uint8(img2*255)


def build_graph(roi, seeds, size=(5, 5, 2)):
    '''Builds and cuts a volumetric image graph'''

    sample = []
    anti_sample = []
    for s in seeds:
        smpl = sample_cut(roi, s, size)
        sample.append(smpl.ravel())
        a_smpl_1 = sample_cut(roi, (size[0], size[1], s[2]), size)
        a_smpl_2 = sample_cut(roi, (roi.shape[0]-size[0]-1, size[1], s[2]), size)
        a_smpl_3 = sample_cut(roi, (size[0], roi.shape[1]-size[1]-1, s[2]), size)
        a_smpl_4 = sample_cut(roi, (roi.shape[0]-size[0]-1, roi.shape[1]-size[1]-1, s[2]), size)
        anti_sample.append(a_smpl_1.ravel())
        anti_sample.append(a_smpl_2.ravel())
        anti_sample.append(a_smpl_3.ravel())
        anti_sample.append(a_smpl_4.ravel())

    # this slows down the function somehow
    # for z in xrange(size[2], roi.shape[2]-size[2]):
    #     a_smpl_1 = sample_cut(roi, (size[0], size[1], z), size)
    #     a_smpl_2 = sample_cut(roi, (roi.shape[0]-size[0]-1, size[1], z), size)
    #     a_smpl_3 = sample_cut(roi, (size[0], roi.shape[1]-size[1]-1, z), size)
    #     a_smpl_4 = sample_cut(roi, (roi.shape[0]-size[0]-1, roi.shape[1]-size[1]-1, z), size)
    #     anti_sample.append(a_smpl_1.ravel())
    #     anti_sample.append(a_smpl_2.ravel())
    #     anti_sample.append(a_smpl_3.ravel())
    #     anti_sample.append(a_smpl_4.ravel())

    prior_fg = roi*gaussian(roi, np.mean(sample), np.std(sample))
    prior_bg = (255-roi)*gaussian(roi, np.mean(anti_sample), np.std(anti_sample))

    # this slows down the function somehow
    # regul = []
    # for z in range(roi.shape[2]):
    #     r = grad_magn(roi[:, :, z])
    #     regul.append(np.max(r)-r)
    # regul = np.array(regul).T

    graph = maxflow.Graph[float]()
    node_ids = graph.add_grid_nodes(roi.shape)
    # temporary fix
    # graph.add_grid_edges(node_ids, regul)
    graph.add_grid_edges(node_ids, 6)
    graph.add_grid_tedges(node_ids, prior_fg, prior_bg)
    graph.maxflow()
    segment = graph.get_grid_segments(node_ids)
    img2 = np.int_(np.logical_not(segment))
    return np.uint8(img2*255)


if __name__ == "__main__":
    sample = np.random.normal(5, 10, size=(50, 50, 3))
    show_gaussian_fit(sample, [10, 20])
