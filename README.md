# CT Vertebra Segmenter
Author: Müller Kristóf (PPKE-ITK MB-BSc) 2019

This project is my BSc thesis

## Description:
This program segments and labels vertebrae in CT images

## Required modules:
* Numpy
* OpenCV
* PyDicom
* PyMaxflow


## How to use:
1. Change the data path in Runner.py
2. Apply your parameters
3. Run Runner.py as a script


## Controls:
* "space" for next vertebra (vertebra window)
* "0,1,2" to change view angle
* "a" to view low quality segments
* "esc" to quit the program
